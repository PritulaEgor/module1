﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {

        }

        public int[] SwapItems(int a, int b)
        {
            int c=a;

            a = b;
            b = c;

            int[] result = new int[]{a, b};
            return result;
        }

        public int GetMinimumValue(int[] input)
        {
            int minimumValue = input[0];

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] < minimumValue)
                {
                    minimumValue = input[i];
                }
            }
            return minimumValue;

        }
    }
}
